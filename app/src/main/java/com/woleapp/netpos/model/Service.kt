package com.woleapp.netpos.model

data class Service(
    var id: Int,
    var serviceName: String,
    var serviceImage: Int
)
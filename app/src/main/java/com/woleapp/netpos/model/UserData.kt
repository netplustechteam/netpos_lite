package com.woleapp.netpos.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserData(
    var businessName: String,
    var partnerName: String,
    var partnerId: String,
    var terminalId: String,
    var terminalSerialNumber: String,
    var businessAddress: String,
    var customerName: String
) : Parcelable

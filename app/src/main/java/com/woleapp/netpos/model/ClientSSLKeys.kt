package com.woleapp.netpos.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "clientSSLKeys", indices = [Index(value = ["serial"], unique = true)])
data class ClientSSLKeys(
    var serial: String,
    var clientCert: String,
    var clientKey: String
){
    @PrimaryKey(autoGenerate = true) var id: Int? = null
}
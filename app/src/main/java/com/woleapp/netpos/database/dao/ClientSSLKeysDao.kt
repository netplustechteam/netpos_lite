package com.woleapp.netpos.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.woleapp.netpos.model.ClientSSLKeys
import io.reactivex.Single


@Dao
interface ClientSSLKeysDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKeys(clientSSLKeys: ClientSSLKeys): Single<Long>

    @Query("SELECT * FROM clientSSLKeys LIMIT 1")
    fun getClientKeys(): Single<ClientSSLKeys>
}
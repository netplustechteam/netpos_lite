package com.woleapp.netpos.util

const val STATE_PAYMENT_STAND_BY = 0
const val STATE_PAYMENT_STARTED = 1
const val STATE_PAYMENT_APPROVED = 2
const val HISTORY_ACTION = "history_action"
const val HISTORY_ACTION_DEFAULT = "Default"
const val HISTORY_ACTION_REPRINT = "Reprint"
const val HISTORY_ACTION_REFUND = "Refund"
const val HISTORY_ACTION_PREAUTH = "PreAuth"
const val HISTORY_ACTION_EOD = "End Of Day"
const val TRANSACTION_TYPE = "transaction_type"
const val PREF_APP_TOKEN = "app_token"
const val PREF_USER_TOKEN = "user_token"
const val PREF_USER = "user"
const val PREF_AUTHENTICATED = "authenticated"
const val PREF_CONFIGURATION_DATA = "configuration_data"
const val PREF_KEYHOLDER = "pref_keyholder"
const val PREF_CONFIG_DATA = "pref_config_data"
const val PREF_LAST_LOCATION = "pref_last_location"
const val PREF_USE_STORM_TERMINAL_ID = "pref_use_storm_tid"
const val PREF_BILLS_TOKEN = "pref_bills_token"
const val PREF_TERMINAL_ID = "pref_terminal_id"
const val PREF_PRINTER_SETTINGS = "pref_printer_settings"
const val PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY = "print_customer_copy_only"
const val PREF_VALUE_PRINT_CUSTOMER_AND_MERCHANT_COPY = "print_merchant_and_customer_copy"
const val PREF_VALUE_PRINT_ASK_BEFORE_PRINTING = "ask_before_printing"
const val PREF_VALUE_PRINT_SMS = "send_sms"
const val ACTION_CONFIGURE = "action_configure"
const val ACTION_SALE = "action_sale"
const val EXTRA_TERMINAL_ID = "extra_terminal_id"
const val EXTRA_BUSINESS_NAME = "extra_business_name"
const val EXTRA_PARTNER_NAME = "extra_partner_name"
const val EXTRA_PARTNER_ID = "extra_partner_id"
const val EXTRA_PHONE_NUMBER = "extra_phone_number"
const val EXTRA_CUSTOMER_NAME = "extra_customer_name"
const val EXTRA_BUSINESS_ADDRESS = "extra_business_address"
const val EXTRA_EMAIL_ADDRESS = "extra_email_address"
const val EXTRA_CLIENT_CERT = "extra_client_cert"
const val EXTRA_CLIENT_KEY = "extra_client_key"
const val EXTRA_AMOUNT = "extra_amount"
const val EXTRA_REMARK = "extra_remark"
const val LAST_POS_CONFIGURATION_TIME = "last_pos_configuration_time"
const val CLIENT_CERT = "-----BEGIN CERTIFICATE-----\n" +
    "MIIFLzCCAxegAwIBAgICEBowDQYJKoZIhvcNAQELBQAwgYYxCzAJBgNVBAYTAk5H\n" +
    "MQ4wDAYDVQQIDAVMYWdvczEQMA4GA1UECgwHTmV0UGx1czEUMBIGA1UECwwLRW5n\n" +
    "aW5lZXJpbmcxHDAaBgNVBAMME2VwbXMubmV0cGx1c3BheS5jb20xITAfBgkqhkiG\n" +
    "9w0BCQEWEmRhcG9Ad2VibWFsbG5nLmNvbTAeFw0yMTA4MjAxMDMwNDBaFw0yMjA4\n" +
    "MzAxMDMwNDBaMGYxCzAJBgNVBAYTAk5HMQ4wDAYDVQQIDAVMYWdvczEOMAwGA1UE\n" +
    "BwwFTGFnb3MxEzARBgNVBAoMClFhcGl0YWxQYXkxEDAOBgNVBAsMB0ZpbmFuY2Ux\n" +
    "EDAOBgNVBAMMB1NjYW5QYXkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB\n" +
    "AQC1WL/Dcs6mUT+qUUIrZBi4W+NoQhAoEKbEeQI8T8uwZY/tuYVJQpCQ2d1Cqxy/\n" +
    "zTg8yNMVWqKWgYgHON0fi+sQIQtFi6LrRZ53BvRUda7p6yMq/xEgmosJxxESbJuZ\n" +
    "+qQLt5sa53uCn2BK9Z1Lvth3HMd5kuNW5kDobuxeuJCeYUfQeOJ41uc4LaVPZQH8\n" +
    "EH8WdbzddJFsisAFcMQSrmFI6dRXuoAmTdbuNyJwMxqqEupekwqorUt6EEUao4cQ\n" +
    "hNPjXo7Z8yiWplvt5hjh+IMCUSQPgId/FIdVCqVEdPAIT7EI3J9knJdST+arq/h0\n" +
    "PEbhb13kWQmZfEhBIzz0+09VAgMBAAGjgcUwgcIwCQYDVR0TBAIwADARBglghkgB\n" +
    "hvhCAQEEBAMCBaAwMwYJYIZIAYb4QgENBCYWJE9wZW5TU0wgR2VuZXJhdGVkIENs\n" +
    "aWVudCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUUDygJwWG7TUTZBSqAAiFjHKNGLUw\n" +
    "HwYDVR0jBBgwFoAUGLroSSS0JgfI+K7bBbGAwXFkAf4wDgYDVR0PAQH/BAQDAgXg\n" +
    "MB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDBDANBgkqhkiG9w0BAQsFAAOC\n" +
    "AgEACWsnibKlfXsRTsmm8lUIMERrngwiMmjQ8j8byPx3H9aPND0ufxCMyGKfNzQN\n" +
    "5WShP8v9eYCqMlF5HhRMSgVQgRjRZGNnqazgnbkT9BzyMeku5dLbQAmkcJBVcmOD\n" +
    "3reVk6NrmLbI6KtvsDUe5Xu0xcXiPCjDO2+cvfu6P2LH+92ZmRk12LPgCctNauiI\n" +
    "pIg3YF6I4xhpYCtHf1bOHV720xr0E8g0fARrhq/eogrYT4mMam8b0FhbOstLg9J0\n" +
    "cRFhTwY51dyyJebId8b6Jv4vQ7eVUwj9msW3u6p6/o2WcLfgbh5r5qpMlW2YFo+/\n" +
    "lJHGMlIBUXA/iXPBimaIOG2UVoOTMWp1uUklqClfMZ2qfLE5cenjMdwn2BrqadKu\n" +
    "ihzmczXVevjW6RKV24EaL9K+h/CqC/fh/rBPXKcTHWkU7HCQD52XluzM3pS8s3uz\n" +
    "s19ew7j3QmUszZk3rF2c+rhouVS7DSa15PS5JieMDbJhVEyd7Pafvh5VbHx+A3Ku\n" +
    "p5AnLjzg6a2ttaZQaP0dK4ZQ80QMUHyiVk9xFfZIDTKrNee77mXPyBbz2SiI8EVf\n" +
    "mESRpxq2Eipe4exsdWWOtfAORmiadkl/7PFhR334c9Bh3BaSJG9EzuAgb0MmWVRa\n" +
    "5eUsIWBgh2Af7dY9QeREXU3rfLBZLIKXvUHEfOT2gnScJ8M=\n" +
    "-----END CERTIFICATE-----"
const val CLIENT_KEY = "-----BEGIN PRIVATE KEY-----\n" +
    "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC1WL/Dcs6mUT+q\n" +
    "UUIrZBi4W+NoQhAoEKbEeQI8T8uwZY/tuYVJQpCQ2d1Cqxy/zTg8yNMVWqKWgYgH\n" +
    "ON0fi+sQIQtFi6LrRZ53BvRUda7p6yMq/xEgmosJxxESbJuZ+qQLt5sa53uCn2BK\n" +
    "9Z1Lvth3HMd5kuNW5kDobuxeuJCeYUfQeOJ41uc4LaVPZQH8EH8WdbzddJFsisAF\n" +
    "cMQSrmFI6dRXuoAmTdbuNyJwMxqqEupekwqorUt6EEUao4cQhNPjXo7Z8yiWplvt\n" +
    "5hjh+IMCUSQPgId/FIdVCqVEdPAIT7EI3J9knJdST+arq/h0PEbhb13kWQmZfEhB\n" +
    "Izz0+09VAgMBAAECggEANM00eKoS9WYuzV5/j7bDY9qNaiviHGhMxmQdfIYXXrMT\n" +
    "pzofTudsxGBLBkBgTJBqUXh/zXXoZT4t5zxtVeQde0LDz2oK88n/i9Sgi38AgOeQ\n" +
    "A9Lkn8OfvCcUF3M8EydhYHADX2ALHEtfdDPzqrxUtsnQYtucCg9c0tCJTkMbCKH5\n" +
    "F6Z1WgrlvX0YwvLcp3thTMhEuDoKYzRUry4hVDTbU1wNcrxtMBJ2TQaufjhH4aHu\n" +
    "1u7FOubjH0V1WGOdNes6B/oB4CITKWEIX43jNwZyZvWgYfQOefxh7g/tDwHu76eu\n" +
    "1dN9/0i7EB0KiU4+CtK4ux//KwqAH6tYA3ZzEt48+QKBgQDYgprNq2BE6Okc5X60\n" +
    "KR/BNvu2nZDFx175p2GhVpWrvDGfjqcjVztrK0Fu4Sz2e4KVvbHj9G2SkzVg4Yxh\n" +
    "WRtOU/iap8/FvqYarf7qGrD/cY8yLRInNGeLOcctlUR1uHfELHzsbyezf4bXBPBR\n" +
    "tOnS8y08zjXPmPdZ1coea2JNtwKBgQDWbEYRJZMvi7weKPjnUqGhuXZNL4MqsVfH\n" +
    "ty7DZc2/gFTrJifxgLOeuLlIfRcSGTw1EBoBTic5h7q2UPUoN9J8zAKgGpStrzAE\n" +
    "tbCtcCKMNpOI+ov+D43jXKCO2rJloVl2I0Io7UNhi3c3VJ4y9HA3DUrtEq5HamB/\n" +
    "kTpj02TLUwKBgQCW9nn57gSyB7SC8YHnHMWHc08Phc3sGZefe5YXaMfzdsUi+9SO\n" +
    "T8SedQqqxVcUhgoHXr0qtMOx7/LIGnxHZFolw2cUbu3wc35eG/QiR3oioou18Lc3\n" +
    "ntKTwTWgEvQpIPZhUqPIN7j273SVeFwYnIxtCiIAw8fBeGb/vJGxpC5nAQKBgQDP\n" +
    "QtFneZ6P8s3fiVMuply96ntDmI19Evb6gUKedSUv2mg69CLf+bulzICsZN2MDQB5\n" +
    "ehro8BZSMbR0zkRqn6rCHqDbCVZ1nEUaCFXDEAUU6xwo56Wx6m8IOlyoORT/z7qd\n" +
    "yu8EiHL9MsYFobFZJ5XgYOTvy4LSXSzxofuCyq8/KQKBgFml2BHeYieCiNdlMs6Z\n" +
    "Wk9+KOtnYN2MX3Ds9t0U+hFX7h8st320GsrnJ+zLu1zcKAAktjbrnPQ7PjHSx3uR\n" +
    "AaU8Z8nyPAN317bu0NjJnlYGlBrOraq/Ag//B5Zt+4g0Cw7sXOy5vORSvhzkKZ7U\n" +
    "TnNxqDO4/zWRES/LbxXr8pX6\n" +
    "-----END PRIVATE KEY-----"

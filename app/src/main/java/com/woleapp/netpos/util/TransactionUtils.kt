package com.woleapp.netpos.util

import com.danbamitale.epmslib.entities.TransactionType
import com.netpluspay.nibssclient.models.IsoAccountType
import com.netpluspay.nibssclient.models.TransactionResponse
import com.netpluspay.nibssclient.models.TransactionWithRemark
import com.netpluspay.nibssclient.models.responseMessage
import com.netpluspay.nibssclient.util.RandomNumUtil.getCurrentDateTime

fun gatewayErrorTransactionResponse(
    amount: Long = 0,
    transactionType: TransactionType = TransactionType.PURCHASE,
    accountType: IsoAccountType = IsoAccountType.DEFAULT_UNSPECIFIED
) = TransactionResponse().apply {
    this.transactionType = transactionType
    this.responseCode = "A5"
    this.RRN = "000000000000"
    this.STAN = "000000"
    this.AID = ""
    this.TSI = ""
    this.TVR = ""
    this.responseMessage
    this.accountType = accountType
    this.transactionTimeInMillis = System.currentTimeMillis()
    this.acquiringInstCode = ""
    this.amount = amount
}

fun mapDanbamitaleResponseToResponse(
    input: TransactionWithRemark,
    inputtedRemark: String
): TransactionResponse {
    return TransactionResponse().apply {
        AID = input.AID
        RRN = input.rrn
        STAN = input.STAN
        TSI = input.TSI
        TVR = input.TVR
        accountType = IsoAccountType.valueOf(input.accountType)
        acquiringInstCode = input.acquiringInstCode
        additionalAmount_54 = input.additionalAmount_54
        amount = input.amount.toLong()
        appCryptogram = input.appCryptogram
        authCode = input.authCode
        cardExpiry = input.cardExpiry
        cardHolder = input.cardHolder
        cardLabel = input.cardLabel
        id = input.id.toLong()
        localDate_13 = input.localDate_13
        localTime_12 = input.localTime_12
        maskedPan = input.maskedPan
        merchantId = input.merchantId
        originalForwardingInstCode = input.originalForwardingInstCode
        otherAmount = input.otherAmount.toLong()
        otherId = input.otherId
        responseCode = input.responseCode
        responseDE55 = input.responseDE55 ?: ""
        terminalId = input.terminalId
        transactionTimeInMillis = input.transactionTimeInMillis
        transactionType = TransactionType.valueOf(input.transactionType)
        transmissionDateTime = getCurrentDateTime()
        remark = inputtedRemark
    }
}

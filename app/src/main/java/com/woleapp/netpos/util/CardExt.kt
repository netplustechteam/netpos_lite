@file:Suppress("DEPRECATION")

package com.woleapp.netpos.util

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.netpluspay.netpossdk.emv.CardReadResult
import com.netpluspay.netpossdk.emv.CardReaderEvent
import com.netpluspay.netpossdk.emv.CardReaderService
import com.netpluspay.nibssclient.models.CardData
import com.netpluspay.nibssclient.models.IsoAccountType
import com.pixplicity.easyprefs.library.Prefs
import com.pos.sdk.emvcore.POIEmvCoreManager.DEV_ICC
import com.pos.sdk.emvcore.POIEmvCoreManager.DEV_PICC
import com.pos.sdk.security.POIHsmManage
import com.woleapp.netpos.R
import com.woleapp.netpos.databinding.DialogSelectAccountTypeBinding
import com.woleapp.netpos.model.CardReaderMqttEvent
import com.woleapp.netpos.nibss.NetPosTerminalConfig
import com.woleapp.netpos.util.Utility.getTerminalId
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.apache.commons.lang.StringUtils
import timber.log.Timber

data class ICCCardHelper(
    val cardReadResult: CardReadResult? = null,
    val customerName: String? = null,
    val cardScheme: String? = null,
    var accountType: IsoAccountType? = null,
    val cardData: CardData? = null,
    val error: Throwable? = null
)

fun showCardDialog(
    context: Activity,
    lifecycleOwner: LifecycleOwner,
    amount: Long,
    cashBackAmount: Long,
    compositeDisposable: CompositeDisposable? = null
): LiveData<Event<ICCCardHelper>> {
    var configurationFinished = false
    val liveData: MutableLiveData<Event<ICCCardHelper>> = MutableLiveData()
    val progressDialog = ProgressDialog(context)
    progressDialog.setMessage("connecting, please wait...")
    var observer: Observer<Event<Int>>? = null
    observer = Observer<Event<Int>> {
        it.getContentIfNotHandled()?.let { int ->
            Timber.e("picked up: $int")
            when (int) {
                0 -> progressDialog.show()
                1 -> {
                    configurationFinished = true
                    if (progressDialog.isShowing)
                        progressDialog.dismiss()
                    getCardLiveData(context, amount, cashBackAmount, liveData, compositeDisposable)
                    NetPosTerminalConfig.liveData.removeObserver(observer!!)
                }
                -1 -> {
                    configurationFinished = true
                    if (progressDialog.isShowing)
                        progressDialog.dismiss()
                    if (getTerminalId().isEmpty()) {
                        Toast.makeText(context, "No TID found on account", Toast.LENGTH_SHORT)
                            .show()
                    } else
                        Toast.makeText(context, "Connection Failed", Toast.LENGTH_SHORT).show()
                }
                else -> {
                }
            }
        }
    }
    NetPosTerminalConfig.liveData.observe(lifecycleOwner, observer)
    if (Prefs.getString("pref_keyholder", "").isNotEmpty()) getCardLiveData(
        context,
        amount,
        cashBackAmount,
        liveData
    )
    return liveData
}

fun getCardLiveData(
    context: Activity,
    amount: Long,
    cashBackAmount: Long,
    liveData: MutableLiveData<Event<ICCCardHelper>>,
    compositeDisposable: CompositeDisposable? = null
) {
    val dialog = ProgressDialog(context)
        .apply {
            setMessage("Waiting for card")
            // setCancelable(false)
        }
    var iccCardHelper: ICCCardHelper? = null
    val cardService = CardReaderService(
        context,
        listOf(DEV_ICC, DEV_PICC),
        keyMode = POIHsmManage.PED_PINBLOCK_FETCH_MODE_TPK
    )
    val c = cardService.initiateICCCardPayment(
        amount,
        cashBackAmount
    )
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
            when (it) {
                is CardReaderEvent.CardRead -> {
                    val cardResult: CardReadResult = it.data

//                    Timber.e(cardResult.iccDataString)
//                    Timber.e(cardResult.nibssIccSubset)
                    val card = CardData(
                        track2Data = cardResult.track2Data!!,
                        nibssIccSubset = cardResult.nibssIccSubset,
                        panSequenceNumber = cardResult.applicationPANSequenceNumber!!,
                        posEntryMode = "051"
                    )
                    cardResult.cardScheme
                    if (cardResult.encryptedPinBlock.isNullOrEmpty().not()) {
                        card.apply {
                            pinBlock = cardResult.encryptedPinBlock
                        }
                    }
                    Timber.e(card.toString())
                    // Timber.e(cardResult.iccDataString)
                    // Timber.e(card.toString())
                    iccCardHelper = ICCCardHelper(
                        cardReadResult = cardResult,
                        customerName = cardResult.cardHolderName,
                        cardScheme = cardResult.cardScheme,
                        cardData = card
                    )
                    val cardReaderMqttEvent = CardReaderMqttEvent(
                        cardExpiry = cardResult.expirationDate,
                        cardHolder = cardResult.cardHolderName,
                        maskedPan = StringUtils.overlay(
                            cardResult.applicationPANSequenceNumber,
                            "xxxxxx",
                            6,
                            12
                        )
                    )
                }
                is CardReaderEvent.CardDetected -> {
                    val mode = when (it.mode) {
                        DEV_ICC -> "EMV"
                        DEV_PICC -> "EMV Contactless"
                        else -> "MAGNETIC STRIPE"
                    }
                    dialog.setMessage("Reading Card with $mode Please Wait")
                    Timber.e("Card Detected")
                }
                else -> {
                }
            }
        }, {
            it?.let {
                dialog.dismiss()
                // sendCardEvent("ERROR", "99", CardReaderMqttEvent(readerError = it.localizedMessage))
                Timber.e("error: ${it.localizedMessage}")
                liveData.value = Event(ICCCardHelper(error = it))
            }
        }, {
            dialog.dismiss()
            showSelectAccountTypeDialog(context, iccCardHelper!!, liveData)
        })

    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Stop") { d, _ ->
        cardService.transEnd(message = "Stopped")
        d.dismiss()
    }
    dialog.show()
    compositeDisposable?.add(c)
}

private fun showSelectAccountTypeDialog(
    context: Activity,
    iccCardHelper: ICCCardHelper,
    liveData: MutableLiveData<Event<ICCCardHelper>>
) {
    Timber.e("show select account dialog")
    var dialogSelectAccountTypeBinding: DialogSelectAccountTypeBinding
    val dialog = AlertDialog.Builder(context)
        .apply {
            dialogSelectAccountTypeBinding =
                DialogSelectAccountTypeBinding.inflate(LayoutInflater.from(context), null, false)
                    .apply {
                        executePendingBindings()
                    }
            setView(dialogSelectAccountTypeBinding.root)
            setCancelable(false)
        }.create()
    dialogSelectAccountTypeBinding.accountTypes.setOnCheckedChangeListener { _, checkedId ->
        val accountType = when (checkedId) {
            R.id.savings_account -> IsoAccountType.SAVINGS
            R.id.current_account -> IsoAccountType.CURRENT
            R.id.credit_account -> IsoAccountType.CREDIT
            R.id.bonus_account -> IsoAccountType.BONUS_ACCOUNT
            R.id.investment_account -> IsoAccountType.INVESTMENT_ACCOUNT
            R.id.universal_account -> IsoAccountType.UNIVERSAL_ACCOUNT
            else -> IsoAccountType.DEFAULT_UNSPECIFIED
        }
        dialog.dismiss()
        Timber.e("$checkedId")
        if (accountType != IsoAccountType.DEFAULT_UNSPECIFIED) {
            iccCardHelper.apply {
                this.accountType = accountType
            }
            liveData.value = Event(iccCardHelper)
        }
    }
    dialogSelectAccountTypeBinding.cancelButton.setOnClickListener {
        dialog.dismiss()
        liveData.value = Event(ICCCardHelper(error = Throwable("Operation was canceled")))
    }
    dialog.show()
}

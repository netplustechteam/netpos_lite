package com.woleapp.netpos.util

import com.google.gson.Gson
import com.netpluspay.netpossdk.NetPosSdk
import com.netpluspay.nibssclient.models.CardData
import com.netpluspay.nibssclient.models.TransactionType
import com.pixplicity.easyprefs.library.Prefs
import com.woleapp.netpos.model.UserData

const val USER_TERMINAL_ID = "USER_TERMINAL_ID"
const val USER_DATA = "USER_DATA"

object Utility {
    fun setTerminalId(terminalId: String) {
        Prefs.putString(USER_TERMINAL_ID, terminalId)
    }

    fun getTerminalId(): String {
        return Prefs.getString(USER_TERMINAL_ID, "")
    }

    fun setUserData(
        businessName: String = "Jumia",
        partnerName: String = "Jumia",
        partnerId: String,
        terminalId: String = "",
        businessAddress: String = "Jumia",
        customerName: String = "Jumia"

    ) {
        val userData = UserData(
            businessName,
            partnerName,
            partnerId,
            terminalId,
            NetPosSdk.getDeviceSerial(),
            businessAddress,
            customerName
        )
        Prefs.putString(USER_DATA, Gson().toJson(userData))
    }

    fun getUserData(): String {
        return Prefs.getString(USER_DATA, "")
    }

    fun mapNetPosCardDataToDanBamitaleCardData(cardData: CardData) =
        com.danbamitale.epmslib.entities.CardData(
            track2Data = cardData.track2Data,
            nibssIccSubset = cardData.nibssIccSubset,
            panSequenceNumber = cardData.panSequenceNumber,
            posEntryMode = cardData.posEntryMode,
        )

    fun TransactionType.mapToDanbamitaleStructure(): com.danbamitale.epmslib.entities.TransactionType =
        com.danbamitale.epmslib.entities.TransactionType.valueOf(this.name)
}

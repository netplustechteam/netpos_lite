package com.woleapp.netpos.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.pos.sdk.printer.POIPrinterManage
import com.woleapp.netpos.databinding.FragmentNipNotificationSearchBinding
import com.woleapp.netpos.model.*
import com.woleapp.netpos.network.StormApiClient
import com.woleapp.netpos.util.print
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class NipNotificationSearch : BaseFragment() {
    private lateinit var binding: FragmentNipNotificationSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNipNotificationSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.nip.root.visibility = View.GONE
        //val userToken = Prefs.getString(PREF_USER_TOKEN, "")
        //val user = Singletons.getCurrentlyLoggedInUser()
        binding.searchButton.setOnClickListener {
            if (binding.sessionCode.text.toString().isEmpty())
                return@setOnClickListener
            StormApiClient.getNipInstance().getNotificationByReference(
                binding.sessionCode.text.toString()
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { binding.progressCircular.visibility = View.VISIBLE }
                .doFinally {
                    binding.progressCircular.visibility = View.GONE
                }
                .subscribe { notification, throwable ->
                    notification?.let {
                        if (it.isNotEmpty()) {
                            binding.nip.nip = it.first()
                            binding.nip.root.visibility = View.VISIBLE
                            binding.nip.print.setOnClickListener { _ ->
                                Timber.e("print nip")
                                it.first().print(requireContext(), object : POIPrinterManage.IPrinterListener{
                                    override fun onError(p0: Int, p1: String?) {
                                        Timber.e("printer error")
                                        Toast.makeText(requireContext(), "Printer Error", Toast.LENGTH_SHORT).show()
                                    }

                                    override fun onFinish() {
                                        Timber.e("on finish")
                                    }

                                    override fun onStart() {
                                        Timber.e("on start")
                                    }

                                })
                            }
                        } else
                            Toast.makeText(
                                requireContext(),
                                "Bank transfer with ref: ${binding.sessionCode.text.toString()} not found",
                                Toast.LENGTH_SHORT
                            ).show()
                    }
                    throwable?.let {
                        Timber.e("Nip Error: ${it.localizedMessage}")
                        Toast.makeText(
                            requireContext(),
                            "Error: ${it.localizedMessage}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }
}
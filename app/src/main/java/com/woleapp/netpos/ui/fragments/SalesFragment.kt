@file:Suppress("DEPRECATION")

package com.woleapp.netpos.ui.fragments

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import com.danbamitale.epmslib.entities.clearPinKey
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.netpluspay.netpossdk.NetPosSdk
import com.netpluspay.netpossdk.utils.DeviceConfig
import com.netpluspay.nibssclient.models.CardData
import com.netpluspay.nibssclient.models.IsoAccountType
import com.netpluspay.nibssclient.models.TransactionType
import com.netpluspay.nibssclient.service.NewNibssApiWrapper
import com.woleapp.netpos.R
import com.woleapp.netpos.databinding.DialogPrintTypeBinding
import com.woleapp.netpos.databinding.DialogTransactionResultBinding
import com.woleapp.netpos.databinding.FragmentSalesBinding
import com.woleapp.netpos.model.Vend
import com.woleapp.netpos.util.*
import com.woleapp.netpos.util.Utility.getUserData
import com.woleapp.netpos.viewmodels.SalesViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.InetSocketAddress
import java.net.Socket
import java.util.concurrent.TimeUnit

class SalesFragment : BaseFragment() {
    companion object {
        fun newInstance(
            transactionType: TransactionType = TransactionType.PURCHASE,
            isVend: Boolean = false,
            amount: Long? = null,
            remark: String? = null
        ): SalesFragment =
            SalesFragment().apply {
                arguments = Bundle().apply {
                    putString(TRANSACTION_TYPE, transactionType.name)
                    putBoolean("IS_VEND", isVend)
                    amount?.let {
                        putLong("amount", it)
                    }
                    remark?.let {
                        putString("remark", it)
                    }
                }
            }
    }

    private val viewModel by viewModels<SalesViewModel>()
    private lateinit var transactionType: TransactionType
    private lateinit var alertDialog: AlertDialog
    private lateinit var receiptDialogBinding: DialogTransactionResultBinding
    private lateinit var dialogPrintTypeBinding: DialogPrintTypeBinding
    private lateinit var printTypeDialog: AlertDialog
    private lateinit var printerErrorDialog: AlertDialog
    private val compositeDisposable = CompositeDisposable()
    private var isVend: Boolean = false
    private lateinit var binding: FragmentSalesBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSalesBinding.inflate(inflater, container, false)
        transactionType = TransactionType.valueOf(
            arguments?.getString(
                TRANSACTION_TYPE,
                TransactionType.PURCHASE.name
            )!!
        )
        isVend = arguments?.getBoolean("IS_VEND", false) ?: false
        viewModel.isVend(isVend)
        receiptDialogBinding = DialogTransactionResultBinding.inflate(inflater, null, false)
            .apply { executePendingBindings() }
        dialogPrintTypeBinding = DialogPrintTypeBinding.inflate(layoutInflater, null, false).apply {
            executePendingBindings()
        }
        printTypeDialog = AlertDialog.Builder(requireContext()).setCancelable(false)
            .apply {
                setView(dialogPrintTypeBinding.root)
                dialogPrintTypeBinding.apply {
                    cancel.setOnClickListener {
                        printTypeDialog.dismiss()
                        viewModel.finish()
                    }
                    customer.setOnClickListener {
                        viewModel.printReceipt(requireContext(), isMerchantCopy = false)
                    }
                    merchant.setOnClickListener {
                        viewModel.printReceipt(requireContext(), isMerchantCopy = true)
                    }
                }
            }.create()
        printerErrorDialog = AlertDialog.Builder(requireContext())
            .apply {
                setTitle("Printer Error")
                setIcon(R.drawable.ic_warning)
                setPositiveButton("Send Receipt") { d, _ ->
                    d.cancel()
                    viewModel.showReceiptDialog()
                }
                setNegativeButton("Dismiss") { d, _ ->
                    d.cancel()
                    viewModel.finish()
                }
            }.create()
        binding.apply {
            viewmodel = viewModel
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
            type = transactionType.name
        }

        viewModel.showReceiptType.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                printTypeDialog.show()
            }
        }
        viewModel.message.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let { s ->
                showSnackBar(s)
            }
        }
        /*viewModel.getCardData.observe(viewLifecycleOwner){event ->
            event.getContentIfNotHandled()?.let {
                quickPay()
            }
        }*/
        viewModel.getCardData.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let { shouldGetCardData ->
                if (shouldGetCardData) {
                    showCardDialog(
                        requireActivity(),
                        viewLifecycleOwner,
                        binding.priceTextbox.text.toString().toLong(),
                        0L,
                        compositeDisposable
                    ).observe(viewLifecycleOwner) { event ->
                        event.getContentIfNotHandled()?.let {
                            it.error?.let { error ->
                                Timber.e(error)
                                Toast.makeText(
                                    requireContext(),
                                    error.message,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            }
                            it.cardData?.let { _ ->
                                viewModel.setCardScheme(it.cardScheme!!)
                                viewModel.setCustomerName(
                                    it.cardReadResult?.cardHolderName ?: "Customer"
                                )
                                viewModel.setAccountType(it.accountType!!)
                                viewModel.cardData = it.cardData
                                viewModel.makePayment(
                                    requireActivity(),
                                    requireContext(),
                                    transactionType
                                )
                            }
                        }
                    }
                }
            }
        }
        viewModel.smsSent.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                receiptDialogBinding.progress.visibility = View.GONE
                receiptDialogBinding.sendButton.isEnabled = true
                if (it) {
                    Toast.makeText(requireContext(), "Sent Receipt", Toast.LENGTH_LONG).show()
                    alertDialog.dismiss()
                    viewModel.finish()
                }
            }
        }
        viewModel.toastMessage.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            }
        }
        viewModel.finish.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                if (it) {
                    if (arguments?.containsKey("amount") == true) {
                        viewModel.lastTransactionResponse.value?.let {
                            requireActivity().setResult(
                                Activity.RESULT_OK,
                                (requireActivity().intent ?: Intent()).apply {
                                    putExtra("message", Singletons.gson.toJson(it))
                                }
                            )
                            requireActivity().finish()
                            return@observe
                        }
                        requireActivity().setResult(
                            Activity.RESULT_CANCELED,
                            (requireActivity().intent ?: Intent()).apply {
                                putExtra("error", "transaction was not processed")
                            }
                        )
                        requireActivity().finish()
                    } else {
                        requireActivity().onBackPressed()
                    }
                }
            }
        }
        viewModel.showPrinterError.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                if (printTypeDialog.isShowing) {
                    printTypeDialog.cancel()
                }
                if (printerErrorDialog.isShowing) {
                    printerErrorDialog.cancel()
                }
                printerErrorDialog.apply {
                    setMessage(it)
                }.show()
            }
        }
        alertDialog = AlertDialog.Builder(requireContext()).setCancelable(false).apply {
            setView(receiptDialogBinding.root)
            receiptDialogBinding.apply {
                closeBtn.setOnClickListener {
                    alertDialog.dismiss()
                    viewModel.finish()
                }
                sendButton.setOnClickListener {
                    if (receiptDialogBinding.telephone.text.toString().length != 11) {
                        Toast.makeText(
                            requireContext(),
                            "Please enter a valid phone number",
                            Toast.LENGTH_LONG
                        ).show()
                        return@setOnClickListener
                    }
                    viewModel.sendSmS(
                        receiptDialogBinding.telephone.text.toString()
                    )
                    progress.visibility = View.VISIBLE
                    sendButton.isEnabled = false
                }
            }
        }.create()
        alertDialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        viewModel.showPrintDialog.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                if (printTypeDialog.isShowing) {
                    printTypeDialog.cancel()
                }
                if (printerErrorDialog.isShowing) {
                    printerErrorDialog.cancel()
                }
                alertDialog.apply {
                    receiptDialogBinding.transactionContent.text = it
                    show()
                }
                receiptDialogBinding.apply {
                    progress.visibility = View.GONE
                    sendButton.isEnabled = true
                }
            }
        }
        viewModel.shouldRefreshNibssKeys.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                if (it && getUserData().isNotEmpty()) {
                    compositeDisposable.add(
                        NewNibssApiWrapper.init(requireContext(), true, getUserData())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { data, error ->
                                data?.let { keyHolderObject ->
                                    keyHolderObject.first?.let { keyHolder ->
                                        NetPosSdk.writeTpkKey(
                                            DeviceConfig.TPKIndex,
                                            keyHolder.clearPinKey
                                        )
                                    }
                                }
                                error?.let { err ->
                                    Timber.e(err)
                                }
                            }
                    )
                }
            }
        }
        binding.process.setOnClickListener {
            viewModel.transactionState.value = STATE_PAYMENT_STARTED
            viewModel.validateField()
        }

        arguments?.let {
            if (it.containsKey("remark") && it.getString("remark").isNullOrEmpty().not()) {
                binding.remark.setText(it.getString("remark"))
                binding.remark.isFocusable = false
            }
            if (it.containsKey("amount")) {
                binding.priceTextbox.setText(it.getLong("amount").toString())
                binding.priceTextbox.isFocusable = false
            }
        }

//        NewNibssApiWrapper.transactionResp.observe(viewLifecycleOwner) {
//            viewModel.lastTransactionResponse.value = mapDanbamitaleResponseToResponse(it)
//            if (Build.MODEL.equals("Pro", true) || Build.MODEL.equals("P3", true)) {
//                Handler().postDelayed({
//                    viewModel.printReceipt(requireContext())
//                }, 1000)
//                requireActivity().setResult(
//                    Activity.RESULT_OK,
//
//                )
//            } else {
//                viewModel.finish()
//            }
//        }

//        if (NewNibssApiWrapper.transactionResp != null) {
//            compositeDisposable.add(
//                NewNibssApiWrapper.transactionResp.subscribeOn(Schedulers.io())
//                    .doOnError {
//                        Log.d("Error", it.localizedMessage)
//                    }
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe { t1, t2 ->
//                        t1?.let { data ->
//                            data?.let {
//                                viewModel.lastTransactionResponse.value =
//                                    mapDanbamitaleResponseToResponse(it)
//                            if (Build.MODEL.equals("Pro", true) || Build.MODEL.equals(
//                                    "P3",
//                                    true
//                                )
//                            ) {
//                                Handler().postDelayed({
//                                    viewModel.printReceipt(requireContext())
//                                    requireActivity().setResult(
//                                        Activity.RESULT_OK,
//                                        (requireActivity().intent ?: Intent()).apply {
//                                            putExtra("message", Gson().toJson(it))
//                                        }
//                                    )
//                                }, 1000)
//                            } else {
//                                Handler().postDelayed({
//                                    viewModel.printReceipt(requireContext())
//                                    viewModel.finish()
//                                }, 2000)
//                            }
//                            }
//                        }
//                    }
//            )
//        }

        return binding.root
    }

/*private fun quickPay() {
    viewModel.setAccountType(IsoAccountType.SAVINGS)
    viewModel.cardData = CardData(
        track2Data = "5199110748591994D2012221000013772F",
        nibssIccSubset = "9F26087BCB3647E84652A29F2701809F10120110A040002A0400000000000000000000FF9F370451271F779F360201E3950500802480009A032009179C01009F02060000000010005F2A020566820239009F1A0205669F34034203009F3303E0F9C89F3501229F1E0831323334353637388407A00000000410109F090200009F03060000000000005F340100",
        panSequenceNumber = "000",
        posEntryMode = "051"
    ).apply {
        pinBlock = TripleDES.encrypt(
            "0425A8EF8B7A6E66",
            NetPosTerminalConfig.getKeyHolder()!!.clearPinKey
        )
    }
    viewModel.makePayment(requireContext(), transactionType)
}*/

    private fun quickPay() {
        viewModel.setAccountType(IsoAccountType.DEFAULT_UNSPECIFIED)
        viewModel.setCardScheme("Verve Card")
        viewModel.setCustomerName("Customer")
        viewModel.cardData = CardData(
            track2Data = "5061041702958403213D2408601013950667", // "5399834599607066D22032210014182625",
            nibssIccSubset = "9F260863EBB0EE7518FB289F2701809F10200FA503A230C0040000000000000000000F0000000000000000000000000000009F3704F2F721749F360200DA950542800480009A032206309C01009F02060000000005005F2A020566820258009F1A0205669F34034203009F3303E068C89F3501229F1E0831313432303136318407A00000037100019F0902008C9F03060000000000005F340101",
            panSequenceNumber = "001",
            posEntryMode = "051"
        )
        viewModel.makePayment(requireActivity(), requireContext(), transactionType)
    }

    override fun onResume() {
        super.onResume()
        viewModel.validateField()
    }

    private fun showSnackBar(message: String) {
        if (message == "Transaction not approved") {
            AlertDialog.Builder(requireContext())
                .apply {
                    setTitle("Response")
                    setMessage(message)
                    show()
                }
        }

        Snackbar.make(
            requireActivity().findViewById(
                R.id.container_main
            ),
            message,
            Snackbar.LENGTH_LONG
        ).show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vend()
    }

    private fun vend() {
        if (isVend) {
            val progressBar = ProgressDialog(context).apply {
                this.setMessage("Waiting for amount.")
                show()
            }
            val socket = Socket()
            var printWriter: PrintWriter? = null
            var reader: BufferedReader? = null
            Observable.fromCallable {
                socket.connect(InetSocketAddress("vend.netpluspay.com", 3535))
                printWriter = PrintWriter(socket.getOutputStream(), true)
                reader = BufferedReader(InputStreamReader(socket.getInputStream()))
                val firstData = reader?.readLine()
                Timber.e(firstData)
            }.flatMap {
                Observable.interval(0, 5, TimeUnit.SECONDS)
            }.flatMap {
                val out = JsonObject().apply {
                    addProperty("serial_number", NetPosSdk.getDeviceSerial())
                    addProperty("status", "")
                }.toString()
                printWriter?.println(out)
                try {
                    val vend = Singletons.gson.fromJson(reader?.readLine(), Vend::class.java)
                    // socket.close()
                    Observable.just(vend)
                } catch (e: Exception) {
                    Observable.just(Vend(0.0))
                }
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.e("vend: $it")
                    if (it.amount > 0.0) {
                        progressBar.dismiss()
                        Toast.makeText(context, "received", Toast.LENGTH_SHORT).show()
                        Toast.makeText(context, it.amount.toString(), Toast.LENGTH_LONG).show()
                        binding.priceTextbox.setText(it.amount.toLong().toString())
                        compositeDisposable.dispose()
                    }
                }, {
                    progressBar.dismiss()
                    Toast.makeText(
                        requireContext(),
                        "Error ${it.localizedMessage}",
                        Toast.LENGTH_SHORT
                    ).show()
                    Timber.e("Error: ${it.localizedMessage}")
                }).disposeWith(compositeDisposable)

            /*Single.fromCallable {
                Socket().run {
                    connect(InetSocketAddress("vend.netpluspay.com", 3535), 30_000)
                    soTimeout = 30_000
                    val reader = BufferedReader(InputStreamReader(getInputStream()))
                    val firstData = reader.readLine()
                    Timber.e(firstData)
                    val printWriter = PrintWriter(getOutputStream(), true)
                    val out = JsonObject().apply {
                        addProperty("serial_number", NetPosSdk.getDeviceSerial())
                        addProperty("status", "")
                    }.toString()
                    printWriter.println(out)
                    reader.readLine()
                }
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {
                    progressBar.dismiss()
                }.flatMap {
                    Timber.e(it)
                    Single.just(Singletons.gson.fromJson(it, Vend::class.java))
                }
                .subscribe { t1, t2 ->
                    t1?.let {
                        Timber.e(it.toString())
                        Toast.makeText(context, "received", Toast.LENGTH_SHORT).show()
                        Toast.makeText(context, it.amount.toString(), Toast.LENGTH_LONG).show()
                        binding.priceTextbox.setText(it.amount.toLong().toString())

                    }
                    t2?.let {
                        Toast.makeText(context, it.localizedMessage, Toast.LENGTH_SHORT).show()
                        Timber.e(it)
                    }
                }.disposeWith(compositeDisposable)*/
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}

@file:Suppress("DEPRECATION")

package com.woleapp.netpos.ui.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.danbamitale.epmslib.entities.clearPinKey
import com.google.gson.Gson
import com.netpluspay.netpossdk.NetPosSdk
import com.netpluspay.netpossdk.utils.DeviceConfig
import com.netpluspay.nibssclient.service.NewNibssApiWrapper
import com.pixplicity.easyprefs.library.Prefs
import com.woleapp.netpos.R
import com.woleapp.netpos.database.AppDatabase
import com.woleapp.netpos.databinding.ActivityMainBinding
import com.woleapp.netpos.model.UserData
import com.woleapp.netpos.nibss.CONFIGURATION_ACTION
import com.woleapp.netpos.ui.fragments.DashboardFragment
import com.woleapp.netpos.ui.fragments.SalesFragment
import com.woleapp.netpos.util.*
import com.woleapp.netpos.util.Utility.getUserData
import com.woleapp.netpos.util.Utility.setTerminalId
import com.woleapp.netpos.util.Utility.setUserData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    private lateinit var progressDialog: ProgressDialog
    private lateinit var alertDialog: AlertDialog
    private lateinit var binding: ActivityMainBinding
    private lateinit var appDatabase: AppDatabase
    private lateinit var userToLogIn: UserData
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    // private lateinit var client: MqttAndroidClient
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                when (it.getIntExtra("terminal_configuration_status", -1)) {
                    0 -> showProgressDialog()
                    1 -> {
                        dismissProgressDialogIfShowing()
                        if (progressDialog.isShowing) {
                            progressDialog.dismiss()
                        }
                        if (this@MainActivity::userToLogIn.isInitialized) {
                            userToLogIn.run {
                                setUserData(
                                    businessName,
                                    partnerName,
                                    partnerId,
                                    terminalId,
                                    businessAddress,
                                    customerName
                                )
                            }
                            Prefs.putString(PREF_USER, Singletons.gson.toJson(userToLogIn))
                            if (intent.action == CONFIGURATION_ACTION) {
                                setResult(
                                    Activity.RESULT_OK,
                                    intent.apply {
                                        putExtra(
                                            "message",
                                            getString(R.string.terminal_configured_successfully)
                                        )
                                    }
                                )
                                this@MainActivity.finish()
                                return
                            } else {
                                performSalesAction()
                                return
                            }
                        } else {
                            setResult(
                                Activity.RESULT_CANCELED,
                                intent.apply {
                                    putExtra("message", getString(R.string.user_data_not_found))
                                }
                            )
                            finish()
                        }
                    }
                    -1 -> {
                        dismissProgressDialogIfShowing()
                        showAlertDialog()
                    }
                }
            }
        }
    }

    private fun showAlertDialog() {
        alertDialog.apply {
            setMessage(getString(R.string.error_configuring_terminal_contact_admin))
            show()
        }
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
    }

    override fun onStart() {
        super.onStart()
        Singletons.getCurrentlyLoggedInUser()?.let {
            binding.dashboardHeader.username.text = it.business_name ?: ""
        }
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(receiver, IntentFilter(CONFIGURATION_ACTION))
    }

    private fun logout() {
        Prefs.remove(PREF_USER_TOKEN)
        Prefs.remove(PREF_AUTHENTICATED)
        Prefs.remove(PREF_KEYHOLDER)
        Prefs.remove(PREF_CONFIG_DATA)
        Prefs.remove(PREF_USER)
        val intent = Intent(this, AuthenticationActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun checkTokenExpiry() {
        val token = Prefs.getString(PREF_USER_TOKEN, null)
        token?.let {
            if (JWTHelper.isExpired(it)) {
                logout()
            }
        }
    }

    private fun dismissProgressDialogIfShowing() {
        progressDialog.dismiss()
    }

    private fun showProgressDialog() {
        progressDialog.show()
    }

    private fun performExternalRequest() {
        when (intent.action) {
            ACTION_CONFIGURE -> {
                performConfigureTerminalAction()
            }
            ACTION_SALE -> {
                performSalesAction()
            }
            else -> {
                Toast.makeText(this, getString(R.string.no_action_passed), Toast.LENGTH_LONG).show()
                setResult(
                    Activity.RESULT_CANCELED,
                    intent.apply {
                        putExtra("error", getString(R.string.no_action_passed_to_intent))
                    }
                )
                finish()
                return
            }
        }
    }

    private fun performConfigureTerminalAction() {
        errorInIntentDataReceived(EXTRA_TERMINAL_ID, getString(R.string.no_tid))
        errorInIntentDataReceived(EXTRA_BUSINESS_NAME, getString(R.string.no_business_name))
        errorInIntentDataReceived(EXTRA_PARTNER_NAME, getString(R.string.no_partner_name))
        errorInIntentDataReceived(EXTRA_PARTNER_ID, getString(R.string.no_partnerId))
        errorInIntentDataReceived(EXTRA_BUSINESS_ADDRESS, getString(R.string.no_bussiness_address))
        errorInIntentDataReceived(EXTRA_CUSTOMER_NAME, getString(R.string.no_customer_name))

        intent?.run {
            userToLogIn = UserData(
                getStringExtra(EXTRA_BUSINESS_NAME)!!,
                getStringExtra(EXTRA_PARTNER_NAME)!!,
                getStringExtra(EXTRA_PARTNER_ID)!!,
                getStringExtra(EXTRA_TERMINAL_ID)!!,
                NetPosSdk.getDeviceSerial(),
                getStringExtra(EXTRA_BUSINESS_ADDRESS)!!,
                getStringExtra(EXTRA_CUSTOMER_NAME)!!
            )
            Prefs.remove(PREF_TERMINAL_ID)
            Prefs.remove(PREF_USER)
            Prefs.remove(PREF_KEYHOLDER)
            setTerminalId(userToLogIn.terminalId)

            progressDialog.show()
            NewNibssApiWrapper.logUser(this@MainActivity, Gson().toJson(userToLogIn))
            compositeDisposable.add(
                NewNibssApiWrapper.init(this@MainActivity, true, Gson().toJson(userToLogIn))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { data, error ->
                        data?.let {
                            it.first?.let { keyHolder ->
                                NetPosSdk.writeTpkKey(DeviceConfig.TPKIndex, keyHolder.clearPinKey)
                            }
                        }
                        error?.let {
                            Timber.e(it)
                        }
                    }
            )
            if (Prefs.getString(PREF_KEYHOLDER, "").isNotEmpty()) {
                Toast.makeText(
                    this@MainActivity,
                    getString(R.string.terminal_configured_successfully),
                    Toast.LENGTH_SHORT
                ).show()
                progressDialog.dismiss()
                setResult(
                    Activity.RESULT_OK,
                    intent.putExtra("message", getString(R.string.terminal_configured_successfully))
                )
                this@MainActivity.finish()
                return
            }
        }
    }

    private fun performSalesAction() {
        val user = Gson().fromJson(getUserData(), UserData::class.java)
        user?.run {
            val amount = try {
                intent.getLongExtra(EXTRA_AMOUNT, 0L)
            } catch (e: Exception) {
                0L
            }
            if (amount < 2) {
                setResult(
                    Activity.RESULT_CANCELED,
                    intent.apply {
                        putExtra("error", getString(R.string.minimum_amount_is_2))
                    }
                )
                finish()
                return
            }
            val remark = intent.getStringExtra(EXTRA_REMARK) ?: ""

            val configurationKeyHolder = Prefs.getString(PREF_KEYHOLDER, "")
            if (configurationKeyHolder.isNotEmpty()) {
                supportFragmentManager.beginTransaction()
                    .replace(
                        R.id.container_main,
                        SalesFragment.newInstance(
                            amount = amount,
                            remark = remark
                        ),
                        SalesFragment::class.java.simpleName
                    )
                    .setCustomAnimations(
                        R.anim.right_to_left,
                        R.anim.left_to_right,
                        R.anim.right_to_left,
                        R.anim.left_to_right
                    )
                    .commit()
                return
            }
        }
        Toast.makeText(
            this,
            getString(R.string.terminal_not_configured),
            Toast.LENGTH_SHORT
        ).show()
        setResult(
            Activity.RESULT_CANCELED,
            intent.apply {
                putExtra("error", getString(R.string.terminal_not_configured))
            }
        )
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        appDatabase = AppDatabase.getDatabaseInstance(applicationContext)
        // loadCerts()
        if (!EasyPermissions.hasPermissions(
                applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.permission_rationale),
                1,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }
        progressDialog = ProgressDialog(this).apply {
            setMessage(getString(R.string.configuring_terminal))
            setCancelable(false)
        }
        alertDialog = AlertDialog.Builder(this).run {
            setCancelable(false)
            title = getString(R.string.message)
            setPositiveButton(getString(R.string.retry)) { dialog, _ ->
                Timber.e(getString(R.string.retrying))
                performExternalRequest()
                dialog.dismiss()
            }
            setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                Toast.makeText(
                    applicationContext,
                    getString(R.string.configuration_cancelled),
                    Toast.LENGTH_LONG
                )
                    .show()
                dialog.dismiss()
                setResult(
                    Activity.RESULT_CANCELED,
                    intent.apply {
                        putExtra("error", getString(R.string.configuration_failed))
                    }
                )
                finish()
                return@setNegativeButton
            }
            create()
        }
//        val user = gson.fromJson(Prefs.getString(PREF_USER, ""), User::class.java)
//        binding.dashboardHeader.username.text = user.business_name
//        binding.dashboardHeader.logout.setOnClickListener {
//            logout()
//        }
        binding.dashboardHeader.logout.visibility = View.GONE
        showFragment(DashboardFragment(), DashboardFragment::class.java.simpleName)
        intent?.let {
            performExternalRequest()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        getLocationUpdates()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
    }

    @SuppressLint("MissingPermission")
    private fun getLocationUpdates() {
        val locationManager = this.getSystemService(LOCATION_SERVICE) as LocationManager
        val locationListener: LocationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                // Called when a new location is found by the network location provider.
                location.let {
                    Prefs.putString(
                        PREF_LAST_LOCATION,
                        "lat:${it.latitude} long:${it.longitude}"
                    )
                    // Timber.e("lat:${it.latitude} long:${it.longitude}")
                }
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                Timber.e("On status changed")
            }

            override fun onProviderEnabled(provider: String) {
                Timber.e("On Provider enabled: $provider")
            }

            override fun onProviderDisabled(provider: String) {
                Timber.e("On Provider disabled $provider")
            }
        }
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            0L,
            0f,
            locationListener
        )
        // locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
    }

    private fun showFragment(targetFragment: Fragment, className: String) {
        try {
            supportFragmentManager.beginTransaction()
                .apply {
                    replace(R.id.container_main, targetFragment, className)
                    setCustomAnimations(R.anim.right_to_left, android.R.anim.fade_out)
                    commit()
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun errorInIntentDataReceived(
        intentToCheck: String,
        messageToSetToReturnedIntent: String
    ) {
        if (intent.hasExtra(intentToCheck).not() || intent.getStringExtra(
                intentToCheck
            ).isNullOrEmpty()
        ) {
            Toast.makeText(this, messageToSetToReturnedIntent, Toast.LENGTH_LONG).show()
            setResult(
                Activity.RESULT_CANCELED,
                intent.apply {
                    putExtra("error", messageToSetToReturnedIntent)
                }
            )
            finish()
            return
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}

package com.woleapp.netpos.nibss

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.netpluspay.nibssclient.models.KeyHolder
import com.netpluspay.nibssclient.models.clearPinKey
import com.pixplicity.easyprefs.library.Prefs
import com.woleapp.netpos.database.AppDatabase
import com.woleapp.netpos.model.ConfigurationData
import com.woleapp.netpos.util.Event
import com.woleapp.netpos.util.PREF_KEYHOLDER
import com.woleapp.netpos.util.PREF_TERMINAL_ID
import com.woleapp.netpos.util.Singletons.getSavedConfigurationData
import io.reactivex.disposables.CompositeDisposable

const val CONFIGURATION_STATUS = "terminal_configuration_status"
const val CONFIGURATION_ACTION = "com.woleapp.netpos.TERMINAL_CONFIGURATION"
const val DEFAULT_TERMINAL_ID = "2057H63U"

object NetPosTerminalConfig {
    private var configurationData: ConfigurationData = getSavedConfigurationData()
    private val disposables = CompositeDisposable()
    private var terminalId: String? = null
    var isConfigurationInProcess = false
    var configurationStatus = -1
    private val mutableLiveData = MutableLiveData(Event(-99))
    val liveData: LiveData<Event<Int>>
        get() = mutableLiveData
    private val sendIntent = Intent(CONFIGURATION_ACTION)

    fun getTerminalId() = terminalId ?: ""
    private fun setTerminalId() {
        terminalId = Prefs.getString(PREF_TERMINAL_ID, "")
    }

    private var keyHolder: KeyHolder? = null
    private lateinit var appDatabase: AppDatabase
    private lateinit var localBroadcastManager: LocalBroadcastManager
    private var configureSilently = false

    private fun configureTerminal(context: Context) {
        Prefs.remove(PREF_KEYHOLDER)
        isConfigurationInProcess = true
        configurationStatus = 0
        sendIntent.putExtra(CONFIGURATION_STATUS, configurationStatus)
        localBroadcastManager.sendBroadcast(sendIntent)
        if (configureSilently.not()) {
            mutableLiveData.postValue(Event(configurationStatus))
            mutableLiveData.postValue(Event(-99))
        }
    }

    private val KeyHolder.configureTerminal
        get() = this.clearPinKey.isNullOrEmpty().not()

    private fun disposeDisposables() {
        disposables.clear()
    }
}

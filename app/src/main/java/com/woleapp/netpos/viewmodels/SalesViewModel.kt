package com.woleapp.netpos.viewmodels

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.netpluspay.netpossdk.printer.PrinterResponse
import com.netpluspay.nibssclient.models.*
import com.netpluspay.nibssclient.service.NewNibssApiWrapper
import com.pixplicity.easyprefs.library.Prefs
import com.woleapp.netpos.R
import com.woleapp.netpos.network.StormApiClient
import com.woleapp.netpos.util.*
import com.woleapp.netpos.util.Utility.getTerminalId
import com.woleapp.netpos.util.Utility.mapNetPosCardDataToDanBamitaleCardData
import com.woleapp.netpos.util.Utility.mapToDanbamitaleStructure
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import timber.log.Timber
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.InetSocketAddress
import java.net.Socket

class SalesViewModel : ViewModel() {
    private var isVend: Boolean = false
    var cardData: CardData? = null
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
    val transactionState = MutableLiveData(STATE_PAYMENT_STAND_BY)
    val lastTransactionResponse = MutableLiveData<TransactionResponse>()
    val amount: MutableLiveData<String> = MutableLiveData<String>("")
    private var amountLong = 0L
    var pin = MutableLiveData("")
    val customerName = MutableLiveData("")
    val remark = MutableLiveData("")
    private var isoAccountType: IsoAccountType? = null
    private var cardScheme: String? = null
    private val _showPrintDialog = MutableLiveData<Event<String>>()
    private var amountDbl: Double = 0.0
    private val _shouldRefreshNibssKeys = MutableLiveData<Event<Boolean>>()
    val shouldRefreshNibssKeys: LiveData<Event<Boolean>>
        get() = _shouldRefreshNibssKeys
    private val _finish = MutableLiveData<Event<Boolean>>()
    private val _showPrinterError = MutableLiveData<Event<String>>()

    val showPrinterError: LiveData<Event<String>>
        get() = _showPrinterError

    val finish: LiveData<Event<Boolean>>
        get() = _finish
    private val _message: MutableLiveData<Event<String>> by lazy {
        MutableLiveData<Event<String>>()
    }
    private val _smsSent = MutableLiveData<Event<Boolean>>()
    val smsSent: LiveData<Event<Boolean>>
        get() = _smsSent

    private val _toastMessage = MutableLiveData<Event<String>>()
    val toastMessage: LiveData<Event<String>>
        get() = _toastMessage
    private val _getCardData = MutableLiveData<Event<Boolean>>()

    val showPrintDialog: LiveData<Event<String>>
        get() = _showPrintDialog

    val getCardData: LiveData<Event<Boolean>>
        get() = _getCardData

    val message: LiveData<Event<String>>
        get() = _message

    fun setCustomerName(name: String) {
        customerName.value = name
    }

    private val _showReceiptTypeMutableLiveData = MutableLiveData<Event<Boolean>>()

    val showReceiptType: LiveData<Event<Boolean>>
        get() = _showReceiptTypeMutableLiveData

    fun validateField() {
        amountDbl = (
            amount.value!!.toDoubleOrNull() ?: kotlin.run {
                _message.value = Event("Enter a valid amount")
                return
            }
            )
        _getCardData.value = Event(true)
    }

    fun makePayment(
        activity: Activity,
        context: Context,
        transactionType: TransactionType = TransactionType.PURCHASE,
    ) {
        transactionState.postValue(STATE_PAYMENT_STARTED)
        val makePaymentParams: MakePaymentParams? =
            cardData?.let {
                mapNetPosCardDataToDanBamitaleCardData(
                    it
                )
            }?.let {
                isoAccountType?.let { it1 ->
                    MakePaymentParams(
                        amount = amountDbl.toLong(),
                        terminalId = getTerminalId(),
                        cardData = it,
                        accountType = it1,
                        transactionType = transactionType.mapToDanbamitaleStructure(),
                        remark = remark.value.toString()
                    )
                }
            }

        cardScheme?.let {
            customerName.value?.let { it1 ->
                compositeDisposable.add(
                    NewNibssApiWrapper.makePayment(
                        context,
                        getTerminalId(),
                        Gson().toJson(makePaymentParams!!),
                        it,
                        it1,
                        remark.value.toString()
                    ).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { t1, t2 ->
                            t1?.let {
                                transactionState.postValue(STATE_PAYMENT_STAND_BY)
                                lastTransactionResponse.value =
                                    mapDanbamitaleResponseToResponse(it, remark.value.toString())
                                if (Build.MODEL.equals("Pro", true) || Build.MODEL.equals(
                                        "P3",
                                        true
                                    )
                                ) {
                                    Handler().postDelayed({
                                        printReceipt(context)
                                        transactionState.postValue(STATE_PAYMENT_STAND_BY)
                                        activity.setResult(
                                            Activity.RESULT_OK,
                                            (activity.intent ?: Intent()).apply {
                                                putExtra("message", Gson().toJson(it))
                                            }
                                        )
                                    }, 1000)
                                } else {
                                    Handler().postDelayed({
                                        showReceiptDialog()
                                    }, 2000)
                                }
                            }
                            t2?.let {
                                transactionState.postValue(STATE_PAYMENT_STAND_BY)
                                Toast.makeText(
                                    context,
                                    context.getString(R.string.error_processing),
                                    Toast.LENGTH_LONG
                                ).show()
                                activity.setResult(
                                    Activity.RESULT_OK,
                                    (activity.intent ?: Intent()).apply {
                                        putExtra("message", Gson().toJson(it))
                                    }
                                )
                                finish()
                            }
                        }
                )
            }
        }
//        Timber.e(cardData.toString())
//        // IsoAccountType.
//        this.amountLong = amountDbl.toLong()
//        Timber.e(transactionType.toString())
//        val requestData = MakePaymentParams(
//            amountLong,
//            0L,
//            cardData!!,
//            transactionType,
//            isoAccountType ?: IsoAccountType.DEFAULT_UNSPECIFIED
//        ).apply {
//            remark = this@SalesViewModel.remark.value
//        }
//        transactionState.value = STATE_PAYMENT_STARTED
//        NibssApiWrapper.makePayment(context, requestData)
//            .flatMap {
//                if (it.responseCode == "A3") {
//                    Prefs.remove(PREF_CONFIG_DATA)
//                    Prefs.remove(PREF_KEYHOLDER)
//                    _shouldRefreshNibssKeys.postValue(Event(true))
//                } else if (it.responseCode == "06") {
//                    val keyHolder = Singletons.getKeyHolder()
//                    keyHolder?.let { key ->
//                        _message.postValue(Event("ReWriting key"))
//                        val tpkResult =
//                            NetPosSdk.writeTpkKey(DeviceConfig.TPKIndex, key.clearPinKey!!)
//                        if (tpkResult != 0) {
//                            Timber.e("write tpk failed")
//                            // Toast.makeText(context, "write tpk failed", Toast.LENGTH_SHORT).show()
//                        } else {
//                            // Toast.makeText(context, "write tpk success", Toast.LENGTH_SHORT).show()
//                            Timber.e("write tpk success")
//                        }
//                    }
//                }
//                if (isVend) {
//                    val j = JsonObject().apply {
//                        addProperty("amount", it.amount)
//                        addProperty("responseCode", it.responseCode)
//                        addProperty("RRN", it.RRN)
//                        addProperty("serial_number", NetPosSdk.getDeviceSerial())
//                    }
//                    sendVendResponse(context, j.toString())
//                }
//
//                // Timber.e(gson.toJson(transactionEvent))
//                // MqttHelper.sendPayload(MqttTopics.TRANSACTIONS, event)
//                it.cardHolder = customerName.value!!
//                it.cardLabel = cardScheme!!
//                lastTransactionResponse.postValue(it)
//                Timber.e(it.toString())
//                Timber.e(it.responseCode)
//                Timber.e(it.responseMessage)
//                _message.postValue(Event(if (it.responseCode == "00") "Transaction Approved" else "Transaction Not approved"))
//                printReceipt(context)
//                AppDatabase.getDatabaseInstance(context).transactionResponseDao()
//                    .insertNewTransaction(it)
//            }
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .doOnError {
//                Timber.e("error thrown")
//                Timber.e(it.localizedMessage)
//            }
//            .doFinally {
//                transactionState.value = STATE_PAYMENT_STAND_BY
//            }.subscribe { t1, throwable ->
//                t1?.let {
//                    // _message.value = Event("")
//                }
//                throwable?.let {
//                    when (it) {
//                        is POSPrinterException -> {
//                            _showPrinterError.value = Event(it.localizedMessage)
//                            _message.value = Event("Error: ${it.localizedMessage}")
//                            Timber.e(it)
//                        }
//                        is UnknownHostException, is SocketException -> {
//                            _message.value =
//                                Event("Connection Error::${it.localizedMessage}")
//                            Timber.e(it.localizedMessage)
//                        }
//                        is NibssClientException -> {
//                            it.nibssError?.let { nibssError ->
//                                Timber.e(nibssError.toString())
//                                if (nibssError.possibleSolution!! == "Attempt configure terminal") {
//                                    Prefs.remove(PREF_CONFIG_DATA)
//                                    Prefs.remove(PREF_KEYHOLDER)
//                                    _shouldRefreshNibssKeys.value = Event(true)
//                                }
//                                _message.value = Event(nibssError.error ?: "Error")
//                            }
//                        }
//                        else -> {
//                            _message.value = Event(it.localizedMessage ?: "Unknown exception")
//                            Timber.e(it)
//                        }
//                    }
//                }
//            }.disposeWith(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun setAccountType(accountType: IsoAccountType) {
        this.isoAccountType = accountType
    }

    fun setCardScheme(cardScheme: String?) {
        this.cardScheme = if (cardScheme.equals("no match", true)) "VERVE" else cardScheme
    }

    fun showReceiptDialog() {
        _showPrintDialog.value = Event(
            lastTransactionResponse.value!!.buildSMSText(remark.value ?: "")
                .toString()
        )
    }

    private fun printReceiptWithinViewModel(context: Context) {
        val transactionResponse = lastTransactionResponse.value ?: gatewayErrorTransactionResponse(
            amountLong,
            TransactionType.PURCHASE.mapToDanbamitaleStructure(),
            isoAccountType!!
        )
            .apply {
                this.cardExpiry = ""
                this.cardHolder = customerName.value ?: ""
            }

        if (Build.MODEL.equals("Pro", true) || Build.MODEL.equals("P3", true)) {
            when (Prefs.getString(PREF_PRINTER_SETTINGS, PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY)) {
                PREF_VALUE_PRINT_CUSTOMER_COPY_ONLY -> printReceipt(context, isMerchantCopy = false)
                PREF_VALUE_PRINT_CUSTOMER_AND_MERCHANT_COPY -> printReceipt(
                    context, printBoth = true
                )
                PREF_VALUE_PRINT_SMS -> _showPrintDialog.postValue(
                    Event(transactionResponse.buildSMSText(remark.value ?: "").toString())
                )
                PREF_VALUE_PRINT_ASK_BEFORE_PRINTING -> _showReceiptTypeMutableLiveData.postValue(
                    Event(true)
                )
            }
        } else
            _showPrintDialog.postValue(
                Event(transactionResponse.buildSMSText(remark.value ?: "").toString())
            )

        if (Build.MODEL.equals("Pro", true) || Build.MODEL.equals(
                "P3",
                true
            )
        ) transactionResponse.print(context, remark.value ?: "")
            .subscribeOn(Schedulers.io()) else {
            _showPrintDialog.postValue(
                Event(
                    transactionResponse.buildSMSText(remark.value ?: "").toString()
                )
            )
            Single.just(PrinterResponse(0, "SMS"))
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t1, t2 ->
            }
            .disposeWith(compositeDisposable)
    }

    fun printReceipt(
        context: Context,
        isMerchantCopy: Boolean = false,
        printBoth: Boolean = false
    ) {
        lastTransactionResponse.value?.print(
            context, remark = remark.value ?: "",
            isMerchantCopy = isMerchantCopy
        )
            ?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe { t1, t2 ->
                t1?.let {
                    if (printBoth) {
                        if (isMerchantCopy)
                            finish()
                        else
                            printReceipt(context, isMerchantCopy = true, printBoth = true)
                    } else
                        finish()
                }
                t2?.let {
                    _showPrinterError.value = Event(it.localizedMessage ?: "Unknown printer error")
                    _message.value = Event("Error: ${it.localizedMessage}")
                    Timber.e(it)
                }
            }?.disposeWith(compositeDisposable)
    }

    fun finish() {
        _finish.value = Event(true)
    }

    fun sendSmS(number: String) {
        val map = JsonObject().apply {
            addProperty("from", "NetPlus")
            addProperty("to", "+234${number.substring(1)}")
            addProperty(
                "message",
                lastTransactionResponse.value!!.buildSMSText(remark.value ?: "").toString()
            )
        }
        Timber.e("payload: $map")
        val auth = "Bearer ${Prefs.getString(PREF_APP_TOKEN, "")}"
        val body: RequestBody = map.toString()
            .toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

        StormApiClient.getSmsServiceInstance().sendSms(auth, body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t1, t2 ->
                t1?.let {
                    _smsSent.value = Event(true)
                    Timber.e("Data $it")
                }
                t2?.let {
                    val httpException = it as? HttpException
                    httpException?.let { _ ->
                    }
                    _smsSent.value = Event(false)
                    _toastMessage.value = Event("Error: ${it.localizedMessage}")
                }
            }.disposeWith(compositeDisposable)
    }

    fun isVend(vend: Boolean) {
        isVend = vend
    }

    private fun sendVendResponse(context: Context, out: String) {
        Single.fromCallable {
            Socket().run {
                connect(InetSocketAddress("vend.netpluspay.com", 3535))
                val reader = BufferedReader(InputStreamReader(getInputStream()))
                Timber.e(reader.readLine())
                val printWriter = PrintWriter(getOutputStream(), true)
                printWriter.println(out)
                reader.readLine()
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally {
            }
            .subscribe { t1, t2 ->
                t1?.let {
                    Timber.e(it)
                    // Toast.makeText(context, "received", Toast.LENGTH_SHORT).show()
                }
                t2?.let {
                    Toast.makeText(context, it.localizedMessage, Toast.LENGTH_SHORT).show()
                    Timber.e(it)
                }
            }.disposeWith(compositeDisposable)
    }
}
